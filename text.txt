curl "https://api.postmarkapp.com/email/batchWithTemplates" \
  -X POST \
  -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -H "X-Postmark-Server-Token: dd837165-417d-4fea-b343-0ec98d8b8b60" \
  -d '{
    "Messages": [
 {
	"From": "maria@mallocprivacy.com",
	"To": "maria@mallocprivacy.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Maria",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
        {
	"From": "maria@mallocprivacy.com",
	"To": "ellen.pao@gmail.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Ellen",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},

 {
	"From": "maria@mallocprivacy.com",
	"To": "furqan@f.inc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Furqan",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "asriskandarajah@gmail.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Arnie",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "juliettesouliman2@gmail.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Juliette",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "alexstavrou@hotmail.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Alex",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "sabihmir@gmail.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Sabih",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "chris@heycharge.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Chris",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "charlesz@alum.mit.edu",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Charles",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "clara@urbaninnovationfund.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Clara",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "jenieri@urbaninnovationfund.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Jenieri",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "julie@urbaninnovationfund.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Julie",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "jitin@dragoncapital.vc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Jitin",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "vic@dragoncapital.vc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Vic",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "team@emmelineventures.vc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Emmeline",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "naseem@emmelineventures.vc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Naseem",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "lakeisha@emmelineventures.vc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "La Keisha",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "azin@emmelineventures.vc",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Azin",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "updates@ycombinator.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "YC",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "Maria.Evgeniou@bankofcyprus.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Maria",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "Maria.Kouloumbri@bankofcyprus.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Maria",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "George.Pelekanos@bankofcyprus.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "George",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "liza@mallocprivacy.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Liza",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "artemis@mallocprivacy.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Artemis",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
 {
	"From": "maria@mallocprivacy.com",
	"To": "d.kallis@mallocprivacy.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Demetris",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
}, {
	"From": "maria@mallocprivacy.com",
	"To": "g.foukaridou@mallocprivacy.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Georgia",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},{
	"From": "maria@mallocprivacy.com",
	"To": "e.charalambous@mallocprivacy.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Evdokia",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},
{
	"From": "maria@mallocprivacy.com",
	"To": "Panayiotis.Korinos@bankofcyprus.com",
	"TemplateAlias": "welcome",
	"TemplateModel": {
		"product_url": "product_url_Value",
		"product_name": "Malloc",
		"name": "Panayiotis",
		"company_name": "Malloc Inc.",
		"company_address": "2261 Market Street #4445, San Francisco, California, 941114, US"
	}
},

    ]
}'
